import json
import xml.etree.ElementTree as ET


def get_dict_words(news, format):
    words_dict = dict()

    for item in news:
        if format == 'json':
            words = item["description"].split()
        else:
            words = item.find("description").text.split()

        for word in words:
            word = word.lower()

            if len(word) > 6:
                if word in words_dict:
                    words_dict[word] += 1
                else:
                    words_dict[word] = 1

    return words_dict


def invert_dict(dictionary):
    inv_dict = dict()

    for item, count in dictionary:
        inv_dict.setdefault(count, [])
        inv_dict[count].append(item)

    return inv_dict


def from_dict_to_sorted_list(dictionary):
    return list(reversed(sorted([int(key) for key in dictionary])))


def get_top_words(words_list, words_dict, format):
    top_words = list()

    for count in words_list:
        for word in words_dict[count]:
            if len(top_words) < 10:
                top_words.append(word)
            else:
                break

    print_words(top_words, format)


def print_words(words, format):
    print(f'###### Формат данных - {format}.\nТоп 10 часто встречающихся слов: {", ".join(words)}')


with open('newsafr.json') as f:
    channel = json.load(f)
    news = channel["rss"]["channel"]["items"]
    format = 'json'

    words_dict = get_dict_words(news, format)
    inv_words_dict = invert_dict(words_dict.items())
    count_words_list = from_dict_to_sorted_list(inv_words_dict.keys())
    get_top_words(count_words_list, inv_words_dict, format)


with open('newsafr.xml') as f:
    tree = ET.parse(f)
    root = tree.getroot()
    news = root.findall("channel/item")
    format = 'xml'

    words_dict = get_dict_words(news, format)
    inv_words_dict = invert_dict(words_dict.items())
    count_words_list = from_dict_to_sorted_list(inv_words_dict.keys())
    get_top_words(count_words_list, inv_words_dict, format)
